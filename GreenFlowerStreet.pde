/**
 * Green Flower Street.
 * It draws something like stained ink with Perlin noise.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2017.10.14
 * updated 2019.09.08
 *  - I threw away PShape.
 *  - use blendMode(DARKEST);
 *  - Refactored.
 *
 */

void setup() {
  size(1080, 1080);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
}

void draw() {
  noiseSeed(floor(random(100)));

  background(0.0, 0.0, 90.0, 100.0);
  translate(0.0, 0.0);

  blendMode(DARKEST);
  drawPoints();
  blendMode(BLEND);
  casing();

  saveFrame("frames/####.png");
  if (frameCount >= 3) {
    exit();
  }
}

/**
 * drawPoints : draw many points
 */
void drawPoints() {

  int patternMax = 6;
  int sizeCntMax = 9;

  noStroke();
  for (int pattern = 0; pattern < patternMax; ++pattern) {
  
    float nsHigStart = random(10.0);
    float nsWidStart = random(10.0);
    float nsRotStart = random(10.0);
    float nsHueStart = random(10.0);
    float nsSatStart = random(10.0);

    for (int sizeCnt = 0; sizeCnt < sizeCntMax; ++sizeCnt) {

      float nsHig = nsHigStart;
      float nsSat = nsSatStart;

      float divH = 1.0;
      float divW = 1.0;

      for (float idxH = height * 0.2; idxH < height * 0.9; idxH += divH) {

        divH = map(noise(nsHig), 0.0, 1.0, 150.0, 300.0); 

        float nsWid = nsWidStart + noise(nsHig);
        float nsRot = nsRotStart + noise(nsHig);
        float nsHue = nsHueStart + noise(nsHig) / 2.0;

        for (float idxW = width * 0.2; idxW < width * 0.9; idxW += divW) {

          divW = map(noise(nsWid), 0.0, 1.0, 0.1, 10.0);

          float brushHue = map(noise(nsHue), 0.0, 1.0, 0.0, 720.0);
          float brushSat = map(noise(nsSat), 0.0, 1.0, 70.0, 90.0) / map(sizeCnt, 0, sizeCntMax, 4.0, 1.0);
          float brushBri = map(pattern, 0, patternMax, 50.0, 80.0);
          float brushAlp = map(noise(nsSat), 0.0, 1.0, 10.0, 40.0) / map(sizeCnt, 0, sizeCntMax, 1.0, 3.0);
          float brushRot = map(noise(nsRot), 0.0, 1.0, -HALF_PI, HALF_PI);
          float brushSiz = divW * divW * (sizeCnt + 1.0) / 3.0;

          pushMatrix();
          translate(idxH - divH, idxW - divW);
          rotate(brushRot);
          translate(divH, divW);
          fill(brushHue % 360.0, brushSat, brushBri, brushAlp);
          ellipse(0.0, 0.0, brushSiz, brushSiz);
          popMatrix();
        
          nsWid += 0.01;
          nsRot += 0.008;
          nsHue += 0.002;

        }

        nsHig += 0.08;
        nsSat += 0.05;

      }
    }
  }
}

/**
 * casing : draw fancy casing
 */
void casing() {
  stroke(0, 0, 100, 100);
  fill(0, 0, 0, 0);
  strokeWeight(50);
  rect(0, 0, width, height);
}
